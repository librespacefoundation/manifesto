**About Us**

**Your trust is important to the Libre Space Foundation and we take privacy and security very seriously. If you have any questions or concerns about our privacy policy or our use of data, please get in touch with us at info@libre.space**

In this text "We" "our" refers to Libre Space Foundation.

Libre Space Foundation staff, management, and volunteers uphold strict internal policies against unauthorized disclosure or use of personal information.

**What information do we collect?**

We collect the information you provide i.e. name and email address and use it only for the purpose of signing up.

We don't store IPs.

**Where do we store the emails?** We store your data internally on secure servers and using a series of technical and organisational security measures to safeguard your personal data and to reduce the risk of loss, misuse, unauthorised access, disclosure and alteration

We will never share your information with parties outside the Libre Space Foundation and we will not use your information for anything else other than signing up the Manifesto. You are free to request to have access to your details or request to remove your details and email address from our Database whenever you wish. All you need to do is drop us an email at info@libre.space

**How do we protect your information?**

We implement a variety of security measures to maintain the safety of your personal information when you enter and submit your personal information.

**What is our data retention policy?** Your data will remain in our Database for 5 years after which it will be deleted.

**Handling the data**

The personal data recipients are the members of the core team of the Libre Space Foundation. The Libre Space Foundation undertakes to handle personal data in compliance with the applicable data protection law, to ensure the security of the data processed, and to implement the appropriate technical and organizational measures for the protection of personal data against unlawful destruction, accidental change, disclosure and any unlawful handling. You have the right to contact the Libre Space Foundation for all matters relating to the processing of your personal data by email to info@libre.space

**Do we use cookies?**

No.

**Do we use Analytics?**

There is no analytics application connected to the page.

**Do we disclose any information to outside parties?**

We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information.

**Online Privacy Policy Only**

This online privacy policy applies only to information collected through the particular site and not to information collected offline or on any other affiliate Libre Space Foundation sites.

**Your Consent**

By using our site and submitting your details you explicitly consent to our web site privacy policy.

**Changes to our Privacy Policy**

If we decide to change our privacy policy, we will post those changes on this page.

This document is CC-BY-SA. It was last updated on July 01, 2020.

**What are your rights?**

1. **The right to be informed**: You have the right to know what type of data is collected, how it’s used, the duration it will be kept and whether it will be shared with any third parties.
2. **The right to access**: You have the right to access the data we hold about you. However, in order for you to gain access to the data, you will be asked to prove your identity. We cannot release any personal data to you without establishing proof of identity. We should be able to provide you with the information you request within a month.
3. **The right to rectification**: If you discover that the information we hold about you is inaccurate or incomplete, you can request an update. Again, we have one month to update your details.
4. **The right to erasure** (**the right to be forgotten**): You have the right to request that we erase your data.
5. **Right to withdraw consent:** You are free to withdraw your consent at any time.
6. **The right to restrict processing**: You can request that we limit the way we use your personal data. However, we are required to keep it to establish, exercise or defend a legal claim.
7. **The right to data portability**: You have the right to obtain the personal data we hold about you in a structured, electronic format.
8. **The right to object**: You can object to the processing of the personal data collected.
9. **The right to complain:** If for any reason you are not happy with the way we have handled your personal data, please contact us at info@libre.space
