*Die Zukunft der Menschheit liegt im All*

Dies eröffnet der Menschheit die Chance auf andere Weise zu forschen, zu entwickeln, zu nutzen und zu gedeihen. Einen Weg zu beschreiten um die Langlebigkeit, Nachhaltigkeit, Offenheit und Gleichheit dieser Bemühungen für die gesamte Menschheit sicherzustellen.

Um dies zu erreichen, verpflichten wir uns zu folgendem:

Prinzipien
----

1. Alle Menschen haben das Recht den Weltraum für das Wohl und im Interesse der gesamten Menschheit zu erforschen und zu nutzen.

2. Die Erkundung und Nutzung des Weltraums erfolgt gemeinschaftlich und kooperativ.

3. Der Weltraum darf nur friedvoll genutzt werden.

4. Finanzieller Nutzen darf nicht die treibende Kraft bei der Erkundung des Weltraums sein.

5. Alle Menschen müssen Zugang zu dem Weltraum, Raumfahrttechnologien und den daraus gewonnen Daten haben.

***

Um diese Prinzipien zu verwirklichen, müssen wir uns dem Folgenden verschreiben:

Säulen
----

1. **Offene Quellen**

    Alle Technologien, welche für den Weltraum entwickelt werden, müssen veröffentlicht und unter einer Open-Source-Lizenz lizensiert werden.

2. **Offene Daten**

    Alle Daten, die im Weltraum oder im Zusammenhang mit ihm gewonnen werden, müssen für jeden und von überall frei zugänglich sein, um auf ihnen aufzubauen und sie im Einklang mit den oben genannten Prinzipien zu teilen und zu verwalten.

3. **Offene Entwicklung**

    Alle Technologien, welche für den Weltraum entwickelt werden, müssen tranparent, verständlich, dokumentiert, testbar, modular und effizient sein.

4. **Offene Regulierung**

    Alle Technologien für den Weltraum müssen unter verteilter und direkter Mitwirkung aller gemeinschaftlich reguliert werden.


***


Support
----

The Manifesto is a bedrock for development in the space age. By adding you or your organization, you can show your support for its principles and pillars.

To join as an organization or individual, sign below.

{{< form-sign action="https://submit-form.com/a7xY2H3c" >}}